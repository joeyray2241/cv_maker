<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="stylesheet" type="text/css" href="/styles/style.css">
	<script type="text/javascript" src="/scripts/script.js" defer></script>
	<title>@yield('title')</title>
</head>
<body>
	<div class="header">
		<p><a href="{{route('login')}}" class="links">Вход</a></p>
		<p><a href="{{route('registration')}}" class="links">Регистрация</a></p>
		<p><a href="{{route('profile')}}" class="links">Личный кабинет</a></p>
		@yield('logout')
	</div>
	<div class="content">
		@yield('content')
	</div>
</body>
</html>