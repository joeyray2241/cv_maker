@extends('layouts.main')

@section('title', 'Личный кабинет')

@section('logout')
	<p><a href="{{route('logout')}}">Выход</a></p>
@endsection

@section('content')
	<h1>Личный кабинет</h1>
	<p>Авторизация прошла успешно</p>
	<a href="{{route('cv_form')}}" class="create_cv">Создать резюме</a>
	<a href="{{route('update_form')}}" class="create_cv">Редактировать мое резюме</a>
	<a href="{{route('print_cv')}}" class="create_cv">Распечатать резюме</a>
	<a href="{{route('create_pdf')}}" class="create_cv">Создать PDF</a>
@endsection