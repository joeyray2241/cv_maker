@extends('layouts.main')

@section('title', 'Регистрация')

@section('content')
	<h1>Регистрация</h1>
	<form method="post" action="{{route('create_user')}}">
		@csrf
		<p>Имя*: <input type="text" name="name" id="name"></p>
		<p>Отчество: <input type="text" name="surname" id="surname"></p>
		<p>Фамилия*: <input type="text" name="lastname" id="lastname"></p>
		<p>Дата рождения*: <input type="text" name="birthdate" id="birthdate"></p>
		<p>Номер телефона*: <input type="text" name="phone_number" id="phone_number"></p>
		<p>E-Mail*: <input type="text" name="email" class="email"></p>
		<p>Пароль: <input type="password" name="password" id="password"></p>
		<p>Повторите пароль: <input type="password" name="check_pass" id="check_pass"></p>
		<input type="submit" id="create_user">
	</form>
	<p id="warning"></p>
@endsection
