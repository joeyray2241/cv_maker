@extends ('layouts.main')

@section ('title', 'Резюме')

@section ('logout')
	<p><a href="{{route('logout')}}">Выход</a></p>
@endsection

@section('content')
	<h1>Редактировать</h1>
	<form method="post" action="{{route('update_cv')}}">
		@csrf
		<h3>Основная информация</h3>
		<input type="text" name="user_id" value="{{$user->id}}" id="id">
		<p>Имя: <input type="text" name="name" value="{{$user->name}}" disabled></p>
		<p>Отчество: <input type="text" name="surname" value="{{$user->surname}}" disabled></p>
		<p>Фамилия: <input type="text" name="lastname" value="{{$user->lastname}}" disabled></p>
		<p>Дата рождения: <input type="text" name="birthdate" value="{{$user->birthdate}}" disabled></p>
		<p>Телефон: <input type="text" name="phone_number" value="{{$user->phone_number}}" disabled></p>
		<p>E-mail: <input type="text" name="email" value="{{$user->email}}" disabled></p>
		<h3>Опыт работы</h3>
		<p>Место работы: <input type="text" name="company" value="{{$curriculum->company}}"></p>
		<p>Должность: <input type="text" name="position" value="{{$curriculum->position}}"></p>
		<p>Период: <input type="text" name="period" value="{{$curriculum->period}}"></p>
		<p>Навыки: <br><textarea name="skills" cols="30" rows="10">{{$curriculum->skills}}</textarea></p>
		<input type="submit" value="Сохранить">
	</form>
@endsection