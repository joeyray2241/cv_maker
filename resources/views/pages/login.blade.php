@extends('layouts.main')

@section('title', 'Вход')

@section('content')
	<h1>Вход</h1>
	<form method="post" action="{{route('check_login')}}">
		@csrf
		<p>E-Mail: <input type="text" name="email"></p>
		<p>Пароль: <input type="password" name="password"></p>
		<input type="submit" name="">
	</form>
@endsection