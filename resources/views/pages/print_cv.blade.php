@extends('layouts.main')

@section('title', 'Распечатать')

@section('content')
	
	<p><b>{{$curriculum->user['lastname']}} {{$curriculum->user['surname']}} {{$curriculum->user['name']}}</b></p>
	<p>Дата рождения: <b>{{$curriculum->user['birthdate']}}</b></p>
	<p>Номер телефона: <b>{{$curriculum->user['phone_number']}}</b></p>
	<p>E-Mail: <b>{{$curriculum->user['email']}}</b></p>
	<p><b>Опыт работы</b></p>
	<p>Место работы: <b>{{$curriculum->company}}</b></p>
	<p>Должность: <b>{{$curriculum->position}}</b></p>
	<p>Период работы: <b>{{$curriculum->period}}</b></p>
	<p><b>Навыки:</b></p>
	<p><b>{{$curriculum->skills}}</b></p>

	<button onclick="window.print()">Распечатать страницу</button>
@endsection