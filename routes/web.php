<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\RegistrationController;
use App\Http\Controllers\LoginController;
use App\Http\Controllers\CurriculumController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('pages.index');
})->name('home');

Route::prefix('user')->group(function () {

	Route::get('profile', function () {
		return view('pages.profile');
	})->middleware('auth')->name('profile');

	Route::get('login', [LoginController::class, "showForm"])->name('login');
	Route::post('login', [LoginController::class, "login"])->name('check_login');
	Route::get('logout', [LoginController::class, "logout"])->name('logout');

	Route::get('registration', [RegistrationController::class, 'showForm'])->name('registration');
	Route::post('create_user', [RegistrationController::class, 'create_user'])->name('create_user');

	Route::get('curriculum_vitae', [CurriculumController::class, 'showForm'])->name('cv_form');
	Route::post('create_cv', [CurriculumController::class, 'create_cv'])->name('create_cv');

	Route::get('update_cv_form', [CurriculumController::class, 'update_form'])->name('update_form');
	Route::post('update_cv', [CurriculumController::class, 'update_cv'])->name('update_cv');

	Route::get('print_cv', [CurriculumController::class, 'print_cv'])->name('print_cv');
	Route::get('create_pdf', [CurriculumController::class, 'create_pdf'])->name('create_pdf');
});
