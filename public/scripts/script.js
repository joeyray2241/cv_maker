
	//при регистрации проверяет поля с паролям, если совпало активирует отправку форму

	let create_user = document.querySelector('#create_user');
	
	if (create_user) {
		create_user.disabled = true;
	}

	let warning = document.querySelector('#warning');
	
	if (warning) {
		warning.innerHTML = "Пароли не совпадают";
	}
	let password = document.querySelector('#password');
	let check_pass = document.querySelector('#check_pass');

	if(password && check_pass) {
		password.addEventListener('keyup', function() {
			if(this.value == check_pass.value) {
				create_user.disabled = false;
				warning.innerHTML = "Пароли совпадают";
			} else {
				create_user.disabled = true;
				warning.innerHTML = "Пароли не совпадают";
			}
		})

		check_pass.addEventListener('keyup', function() {
			if(this.value == password.value) {
				create_user.disabled = false;
				warning.innerHTML = "Cовпадают";
			} else {
				create_user.disabled = true;
				warning.innerHTML = "Пароли не совпадают";
			}
		})
	}

	//костыль, убирающий ссылки со страницы с резюме для печати

	let title = document.querySelector('title');

	if(title.innerHTML.toLowerCase() == "распечатать") {
		let links = document.querySelectorAll('a');
		
		for (let i = 0; i < links.length; i++) {
			links[i].style.display = 'none';
		}
	}