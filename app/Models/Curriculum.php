<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Curriculum extends Model
{
    use HasFactory;

    protected $fillable = [
    	'user_id', 
    	'company', 
    	'position', 
    	'period', 
    	'skills'
    ];

     protected $table = 'curriculum';

    public function user() {

    	return $this->belongsTo(User::class);
    }
}
