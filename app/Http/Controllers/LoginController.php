<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller
{
    public function showForm() {
    	if(Auth::check()) {
    		return redirect(route('profile'));
    	}

    	return view('pages.login');
    }

    public function login(Request $request) {
    	$fields = $request->only('email', 'password');

    	if(Auth::attempt($fields)) {
    		return redirect()->intended(route('profile'));
    	}
    }

    public function logout() {
    	Auth::logout();
    	return redirect(route('home'));
    }
}
