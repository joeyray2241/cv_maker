<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use Illuminate\Support\Facades\Auth;

class RegistrationController extends Controller
{
    public function showForm() {
    	if(Auth::check()) {
    		return redirect(route('profile'));
    	}

    	return view('pages.registration');
    }

    public function create_user(Request $request) {
    	$validated = $request->validate([
    		'name' => 'required',
    		'surname' => 'required',
    		'lastname' => 'required',
    		'birthdate' => 'required',
    		'phone_number' => 'required',
    		'email' => 'required',
    		'password' => 'required'
    	]);
    	
    	$user = User::create($validated);

    	if($user) {
    		Auth::login($user);
    		return redirect(route('profile'));
    	}
    }
}
