<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Curriculum;
use Illuminate\Support\Facades\Auth;
use Codedge\Fpdf\Fpdf\Fpdf;

class CurriculumController extends Controller
{
    public function showForm() {
    	$user =  Auth::user();;

    	return view('pages.cv', [
    		"user" => $user
    	]);
    }

    public function update_form() {
        $user =  Auth::user();
        $curriculum = Curriculum::where('user_id', $user->id)->first();


        if ($curriculum) {
            return view('pages.update_cv_form', [
                "user" => $user,
                "curriculum" => $curriculum
            ]);
        };
        
        return redirect(route('cv_form'));
    }

    public function print_cv() {
        $user =  Auth::user();
        $curriculum = Curriculum::where('user_id', $user->id)->first();

        if ($curriculum) {
            return view('pages.print_cv', [
                "user" => $user,
                "curriculum" => $curriculum
            ]);
        };
        
        return redirect(route('cv_form'));
    }

    public function create_cv(Request $request) {
    	$fileds = $request->only('user_id', 'company', 'position', 'period', 'skills');

    	$cv = Curriculum::create($fileds);

    	if($cv) {
    		return redirect(route('profile'));
    	}   	
    }

    public function update_cv(Request $request) {
        $user = Auth::user();
        $fields = $request->only('company', 'position', 'period', 'skills');
        $update = Curriculum::where('user_id', $user->id)->update($fields);

        if($update) {
            return redirect(route('profile'));
        }
    }

    public function create_pdf() {

        $user = Auth::user();
        $curriculum = Curriculum::where('user_id', $user->id)->first();

        if(!$curriculum) {
            return redirect(route('cv_form'));
        }

        $name = $user->lastname.' '. $user->surname.' '. $user->name;
        $birthdate = 'Дата рождения: '.$user->birthdate;
        $phone_number = 'Номер телефона: '.$user->phone_number;
        $email = 'E-Mail: '.$user->email;
        $company = "Место работы: ".$curriculum->company;
        $position = "Должность: ".$curriculum->position;

        $pdf = new FPDF();
        $pdf->AddFont('TimesNewRomanPSMT', '', 'times.php');
        
        $pdf->AddPage();
        $pdf->SetFont('TimesNewRomanPSMT','',16);
        $pdf->Cell(190,25, $name, 0, 0, 'C');
        $pdf->Ln(10);
        $pdf->Cell(190,25, $birthdate);
        $pdf->Ln(10);
        $pdf->Cell(190,25, $phone_number);
        $pdf->Ln(10);
        $pdf->Cell(190,25, $email);
        $pdf->Ln(10);
        $pdf->Cell(190,25, 'Опыт работы');
        $pdf->Ln(10);
        $pdf->Cell(190,25, $position);
        $pdf->Ln(10);
        $pdf->Cell(190,25, $company);
        $pdf->Ln(10);
        $pdf->Cell(190,25, $position);
        $pdf->Ln(10);
        $pdf->Cell(190,25, 'Навыки:');
        $pdf->Ln(10);
        $pdf->Cell(190,25, $curriculum->skills);


        $pdf->Output();
        exit();
    }
}
